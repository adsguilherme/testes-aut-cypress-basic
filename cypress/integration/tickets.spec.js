/// <reference types="cypress" />

beforeEach(() => {
  cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html')
})

afterEach(() => {
  cy.screenshot()
})

context('Tickets', () => {

  it('Preenchendo todos os inputs de texto', () => {

    const FIRST_NAME = 'Guilherme'
    const LAST_NAME = 'de Sousa'

    cy.get('#first-name').type(FIRST_NAME)
    cy.get('#last-name').type(LAST_NAME)
    cy.get('#email').type('gscode@gscode.com')
    cy.get('#requests').type('Vegetarian')
    cy.get('#signature').type(`${FIRST_NAME} ${LAST_NAME}`)
  })

  it('Selecione dois tickets', () => {
    cy.get('#ticket-quantity').select('2')
  })

  it('Selecione o Ticket Type VIP', () => {
    cy.get('#vip').check()
  })

  it('Selecione o checkbox Social Media', () => {
    cy.get('#social-media').check()
  })

  it('Selecione Friend e Publication e desmarque Friend ', () => {
    cy.get('#friend').check()
    cy.get('#publication').check()
    cy.get('#friend').uncheck()
  })

  it("Tem o texto 'TICKETBOX' no header", () => {
    cy.get('header h1').should('contain', 'TICKETBOX')
    cy.url().should('contain', 'ticket-box')
  })

  it('Alerta de email inválido', () => {
    
    cy.get('#email').type('gscode-gscode.com')
    
    cy.get('#email.invalid').should('exist') // Verificando se o elemento possui a classe invalid
    
    cy.get('#email')
      .clear()
      .type('gscode@gscode.com')

      cy.get('#email.valid').should('not.exist') // Digitando email válido e verificando que a classe invalid não existe
  })

  it('Preencher os campos e resetar o formulário', () => {
    const FIRST_NAME = 'Guilherme'
    const LAST_NAME = 'de Sousa'
    const FULL_NAME = `${FIRST_NAME} ${LAST_NAME}`

    cy.get('#first-name').type(FIRST_NAME)
    cy.get('#last-name').type(LAST_NAME)
    cy.get('#email').type('gscode@gscode.com')
    cy.get('#ticket-quantity').select('2')
    cy.get('#vip').check()
    cy.get('#friend').check()
    cy.get('#requests').type('Pizza')

    cy.get('.agreement p').should('contain', `I, ${FULL_NAME}, wish to buy 2 VIP tickets.`)

    cy.get('#agree').click()
    cy.get('#signature').type(FULL_NAME) // Reaproveitamento com o uso da CONST

    cy.get("button[type='submit']")
      .as('subimitButton')
      .should('not.be.disabled')

    cy.get("button[type='reset']").click()

    //cy.get('.success p').should('contain', 'Ticket(s) successfully ordered.') // Após clicar no botão Confir Ticket é gerado um texto de sucesso da compra.

    cy.get('@subimitButton').should('be.disabled')

  })

  it('Preenchendo campos obrigatórios utilizando Custom Commands', () => {
    // Criando um objeto JS chave e valor
    // Com o uso de arquivos de massa de dados na Fixture, não precisaria criar dentro da spec esse objeto.
    // E também não ficaria confuso entender a chamada do feita pelo command com passagem de parametro (linha 101)
    const CUSTOMER = {
      firstName: "John",
      lastName: "Connor",
      email: "johnconnor@gmail.com"
    }

    cy.preencherCamposObrigatórios(CUSTOMER)

    cy.get("button[type='submit']")
      .as('subimitButton')
      .should('not.be.disabled')

    cy.get('#agree').uncheck()

    cy.get('@subimitButton').should('be.disabled')

  })
  

})