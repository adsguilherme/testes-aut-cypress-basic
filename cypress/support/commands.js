// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })


// Neste exemplo poderia ter criado um arquivo de massa de dados (dados.json) dentro da pasta Fixtures.
// Fazer o import desse arquivo aqui dentro de commands.js.
// E por fim fazer a chamada desse Command dentro do arquivo ticket.spec.js
Cypress.Commands.add("preencherCamposObrigatórios", (CUSTOMER) => {
  cy.get("#first-name").type(CUSTOMER.firstName)
  cy.get("#last-name").type(CUSTOMER.lastName)
  cy.get("#email").type(CUSTOMER.email)
  cy.get('#agree').click()

})
